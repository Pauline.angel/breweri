package br.com.dbserver.tabchallange.engelsdorff.breweri.dto;

public class Beer {

    public static Long KEY_ID_BEER_CONTROLLER = 0L;

    private Long beerId;
    private BeerType beerType;
    private String beerTypeName;
    private int temp;
    private String msg;

    public Long getBeerId() {
        return beerId;
    }

    public void setBeerId(Long id) {
        this.beerId = id;
    }

    public BeerType getBeerType() {
        return beerType;
    }

    public void setBeerType(BeerType beerType) {
        this.beerType = beerType;
    }

    public String getBeerTypeName() {
        return beerTypeName;
    }

    public void setBeerTypeName(String beerTypeName) {
        this.beerTypeName = beerTypeName;
    }

    public int getTemp() {
        return temp;
    }

    public void setTemp(int temp) {
        this.temp = temp;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
