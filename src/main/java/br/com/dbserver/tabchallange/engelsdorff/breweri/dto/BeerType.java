package br.com.dbserver.tabchallange.engelsdorff.breweri.dto;

public class BeerType {

    private String beerTypeName;
    private int minTemp;
    private int maxTemp;

    public String getBeerTypeName() {
        return beerTypeName;
    }

    public void setBeerTypeName(String beerTypeName) {
        this.beerTypeName = beerTypeName;
    }

    public int getMinTemp() {
        return minTemp;
    }

    public void setMinTemp(int minTemp) {
        this.minTemp = minTemp;
    }

    public int getMaxTemp() {
        return maxTemp;
    }

    public void setMaxTemp(int maxTemp) {
        this.maxTemp = maxTemp;
    }

    public static BeerType CreateBeerType(String beerTypeName) {
        BeerType newBeerType = new BeerType();
        if ("Pilsner".equals(beerTypeName)) {
            newBeerType.setBeerTypeName("Pilsner");
            newBeerType.setMinTemp(-4);
            newBeerType.setMaxTemp(-6);
        } else if ("IPA".equals(beerTypeName)) {
            newBeerType.setBeerTypeName("IPA");
            newBeerType.setMinTemp(-5);
            newBeerType.setMaxTemp(-6);
        } else if ("Lager".equals(beerTypeName)) {
            newBeerType.setBeerTypeName("Lager");
            newBeerType.setMinTemp(-4);
            newBeerType.setMaxTemp(-7);
        } else if ("Stout".equals(beerTypeName)) {
            newBeerType.setBeerTypeName("Stout");
            newBeerType.setMinTemp(-6);
            newBeerType.setMaxTemp(-8);
        } else if ("Wheat beer".equals(beerTypeName)) {
            newBeerType.setBeerTypeName("Wheat beer");
            newBeerType.setMinTemp(-3);
            newBeerType.setMaxTemp(-5);
        } else if ("Pale Ale".equals(beerTypeName)) {
            newBeerType.setBeerTypeName("Pale Ale");
            newBeerType.setMinTemp(-4);
            newBeerType.setMaxTemp(-6);
        } else {
            newBeerType.setBeerTypeName("Unknown");
        }
        return newBeerType;
    }
}
