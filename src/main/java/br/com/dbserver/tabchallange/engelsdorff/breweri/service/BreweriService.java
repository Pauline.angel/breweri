package br.com.dbserver.tabchallange.engelsdorff.breweri.service;

import br.com.dbserver.tabchallange.engelsdorff.breweri.dto.Beer;
import br.com.dbserver.tabchallange.engelsdorff.breweri.dto.BeerContainer;
import br.com.dbserver.tabchallange.engelsdorff.breweri.dto.BeerType;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

//TODO: create the proper exceptions
//TODO: review de beer container control
//TODO: add the new services to the rest controller
//TODO: check why swagger is not working
//TODO: THE FUCKING WEB!!!!!


@Service
public class BreweriService {

    List<BeerContainer> beerContainersList = new ArrayList<>();

    //Container methods

    public BeerContainer createBeerContainer() {
        BeerContainer container = new BeerContainer();
        container.setContainerId(BeerContainer.KEY_ID_CONTAINER_CONTROLLER++);
        beerContainersList.add(container);
        return container;
    }

    public BeerContainer getBeerContainer(Long containerId) {
        return beerContainersList.stream().filter(beerContainer -> beerContainer.getContainerId().equals(containerId)).findAny().orElse(null);
    }

    public List<BeerContainer> getAllContainers() {
        return beerContainersList;
    }


    //Beer methods

    public List<Beer> getAllBeersFromContainer(Long containerId) {
        return this.getBeerContainer(containerId).getBeerList();
    }

    public Beer createBeer(Long containerId, Beer beer) throws Exception {
        beer.setBeerId(Beer.KEY_ID_BEER_CONTROLLER++);
        if (!StringUtils.isEmpty(beer.getBeerTypeName())) {
            beer.setBeerType(BeerType.CreateBeerType(beer.getBeerTypeName()));
        }
        this.getBeerContainer(containerId).addBeerToContainer(beer);
        return beer;
    }

    public Beer getBeer(Long containerId, Long beerId) {
        BeerContainer container = this.getBeerContainer(containerId);
        return container.getBeer(beerId);
    }

    public Beer updateBeer(Long containerId, Beer beer) {
        Beer beerRet = this.getBeer(beer.getBeerId(), containerId);
        beerRet.setBeerType(beer.getBeerType());
        beerRet.setTemp(beer.getTemp());

        return beerRet;
    }

    public Beer removeBeer(Long containerId, Long beerId) {
        Beer beerRet = this.getBeer(containerId, beerId);
        this.getBeerContainer(containerId).getBeerList().remove(beerRet);
        return beerRet;
    }

    public BeerContainer openContainderDoor(Long containerId) {
        BeerContainer container = this.getBeerContainer(containerId);
        container.openDoor();
        return container;
    }

    public BeerContainer closeContainderDoor(Long containerId) {
        BeerContainer container = this.getBeerContainer(containerId);
        container.closeDoor();
        return container;
    }
}