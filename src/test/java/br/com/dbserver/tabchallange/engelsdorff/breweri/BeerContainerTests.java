package br.com.dbserver.tabchallange.engelsdorff.breweri;

import br.com.dbserver.tabchallange.engelsdorff.breweri.dto.Beer;
import br.com.dbserver.tabchallange.engelsdorff.breweri.dto.BeerContainer;
import br.com.dbserver.tabchallange.engelsdorff.breweri.dto.BeerType;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BeerContainerTests {

    BeerContainer container = new BeerContainer();

    /***
     * Testing the raise temperature logic from the container
     */
    @Test
    public void testRaisingTemperature() {
        float now = container.getTemp();
        container.openDoor();
        try {
            Thread.sleep(2000);
        } catch (Exception exp) {
            exp.printStackTrace();
        }
        container.closeDoor();
        assertTrue("Temperature raised", now < container.getTemp());
    }

    /***
     * Testing the lower temperature logic from the container
     */
    @Test
    public void testLoweringTemperature() {
        container.openDoor();
        try {
            Thread.sleep(2000);
        } catch (Exception exp) {
            exp.printStackTrace();
        }
        container.closeDoor();
        float now = container.getTemp();
        try {
            Thread.sleep(2000);
        } catch (Exception exp) {
            exp.printStackTrace();
        }
        container.openDoor();
        assertTrue("Temperature lowered", now > container.getTemp());
    }

    /***
     * Testing the update msg status for beers depeding on the container temperature, for this case too hot msg
     */
    @Test
    public void testHotMessage() {
        Beer beer = new Beer();
        beer.setBeerType(BeerType.CreateBeerType("IPA"));
        try{
            container.addBeerToContainer(beer);
        } catch (Exception exp) {
            exp.printStackTrace();
        }
        container.setTemp(-2f);
        container.closeDoor();
        System.out.println(container.getTemp());
        assertEquals("Msg should be hot!", "Too hot for this beer!", beer.getMsg());
    }

    /***
     * Testing the update msg status for beers depeding on the container temperature, for this case too cold msg
     */
    @Test
    public void testColdMessage() {
        Beer beer = new Beer();
        beer.setBeerType(BeerType.CreateBeerType("Stout"));
        try{
            container.addBeerToContainer(beer);
        } catch (Exception exp) {
            exp.printStackTrace();
        }
        container.setTemp(-10f);
        container.closeDoor();
        assertEquals("Msg should be cold!", "Too cold for this beer!",beer.getMsg());
    }
}
